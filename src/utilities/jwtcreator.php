<?php
use Carbon\Carbon;
/**
 * creates a web token
 * credit: https://dev.to/oktadev/create-and-verify-jwts-in-php-with-oauth-2-0-5713
 */
class jwtcreator
{

    public static function getToken(string $user, string $role)
    {
        $secret = getenv('SECRET');
        // Create the token header
        $header = json_encode([
            'typ' => 'JWT',
            'alg' => 'HS256'
        ]);
        
        // Create the token payload
        $payload = json_encode([
            'user_id' => $user,
            'role' => $role,
            'exp' => 1593828222
        ]);
        // Encode Header
        $base64UrlHeader = misc::base64UrlEncode($header);
        // Encode Payload
        $base64UrlPayload = misc::base64UrlEncode($payload);
        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
        // Encode Signature to Base64Url String
        $base64UrlSignature = misc::base64UrlEncode($signature);
        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
        return $jwt;
    }

    /**
     * verify token
     */
    public static function verifyToken(string $token)
    {
        $secret = getenv('SECRET');
        if(strpos($token, '.') <= 0)
        {
            throw new invalidsignexception();
        }
        $tokenParts = explode('.', $token);
        if(count($tokenParts) < 3)
        {
            throw new invalidsignexception();
        }
        $header = base64_decode($tokenParts[0]);
        $payload = base64_decode($tokenParts[1]);
        $signatureProvided = $tokenParts[2];
        
        // check the expiration time - note this will cause an error if there is no 'exp' claim in the token
        $expiration = Carbon::createFromTimestamp(json_decode($payload)->exp);
        $tokenExpired = (Carbon::now()->diffInSeconds($expiration, false) < 0);
        
        // build a signature based on the header and payload using the secret
        $base64UrlHeader = misc::base64UrlEncode($header);
        $base64UrlPayload = misc::base64UrlEncode($payload);
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
        $base64UrlSignature = misc::base64UrlEncode($signature);
        
        // verify it matches the signature provided in the token
        $signatureValid = ($base64UrlSignature === $signatureProvided);
        
        echo "Header:\n" . $header . "\n";
        echo "Payload:\n" . $payload . "\n";
        if ($signatureValid) {
            echo "The signature is valid.\n";
        } else {
            throw new invalidsignexception();
        }
        
        if ($tokenExpired) {
            throw new tokenexpiredexception();
        } else {
            echo "Token has not expired yet.\n";
        }        
    }    
}