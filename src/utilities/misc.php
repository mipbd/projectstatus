<?php
class misc
{
    // PHP has no base64UrlEncode function, so let's define one that
    // does some magic by replacing + with -, / with _ and = with ''.
    // This way we can pass the string within URLs without
    // any URL encoding.
    //credit - https://dev.to/oktadev/create-and-verify-jwts-in-php-with-oauth-2-0-5713
    public static function base64UrlEncode($text)
    {
        return str_replace(
            ['+', '/', '='],
            ['-', '_', ''],
            base64_encode($text)
        );
    }
}