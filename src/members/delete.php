<?php
 
// include database and object file
include_once '../config/dbconnection.php';
include_once '../objects/member.php';
  
// get database connection
$database = new dbconnection();
$db = $database->getConnection();
  
// prepare member object
$member = new member($db);
  
// get member id
$data = json_decode(file_get_contents("php://input"));
  
// set member id to be deleted
$member->id = $data->id;
  
// delete the memeber
if($member->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Member was deleted."));
}
  
// if unable to delete the member
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete member."));
}
