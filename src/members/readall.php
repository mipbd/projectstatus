<?php
include_once '../config/dbconnection.php';
include_once '../objects/member.php';

// instantiate database and member object
$database = new dbconnection();
$db = $database->getConnection();

//initialize object

$member = new member($db);

//read members will be here
// query members

$statement = $member->readall();
$num = $statement->rowCount();

//check if more thar 0 records found

if ($num > 0) {
    //member array

    $member_array = array();
    $member_array["records"] = array();

    //retrieve our table contents
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
        //extract row 
        //this will make $row['member_name'] to just $member_name
        extract($row);

        $member_field = array(
            "id" => $id,
            "member_name" => $member_name,
            "member_email" => $member_email,
            "member_phone" => $member_phone,
            "member_password" => $member_password,
            "member_role" => $member_role,
            "member_status" => $member_status
        );
        array_push($member_array["records"], $member_field);
    }
    // set response code - 200 OK
    http_response_code(200);

    // show members data in json format
    echo json_encode($member_array);
}

// no members found will be here
else {

    // set response code - 404 Not found
    http_response_code(404);

    // tell the user no members found
    echo json_encode(
        array("message" => "No member found.")
    );
}
