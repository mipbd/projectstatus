<?php
include_once '../config/dbconnection.php';
include_once '../objects/member.php';

//get database connection
$database = new dbconnection();
$db = $database->getConnection();


//prepare member object
$member = new member($db);

//get id of the member to be edited

$data = json_decode(file_get_contents("php://input"));

//set id property of member to be edited

$member->id = $data->id;

//set property  values of member 

$member->member_name = $data->member_name;
$member->member_email = $data->member_email;
$member->member_phone = $data->member_phone;
$member->member_password = $data->member_password;
$member->member_role = $data->member_role;
$member->member_status = $data->member_status;



//update member information

if ($member->update()) {

    //set response to - 200 ok
    http_response_code(200);

    //tell the user 
    echo json_encode(array("message" => " Member is updated."));
}

//if unable to update the member , tell the user

else {
    //set response code -503 service unavailable
    http_response_code(503);

    //tell the user
    echo json_encode(array("message" => "Unable to update the member."));
}
