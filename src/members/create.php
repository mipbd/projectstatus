<?php

include_once '../config/dbconnection.php';
include_once '../objects/member.php';

$database = new dbconnection();
$db = $database->getConnection();

$member = new member($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->member_name) &&
    !empty($data->member_email) &&
    !empty($data->member_phone) &&
    !empty($data->member_password) &&
    !empty($data->member_role) &&
    !empty($data->member_status)
) {
    $member->member_name = $data->member_name;
    $member->member_email = $data->member_email;
    $member->member_phone = $data->member_phone;
    $member->member_password = $data->member_password;
    $member->member_role = $data->member_role;
    $member->member_status = $data->member_status;

    if ($member->create()) {
        http_response_code(201);
        echo json_encode(array("member created")); ////
    } else {
        http_response_code(503);
        echo json_encode(array("unable to create member"));
    }
} else {
    http_response_code(400);
    echo json_encode(array("Unable to create member. Data is incomplete"));
}
