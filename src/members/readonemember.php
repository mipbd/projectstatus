<?php
include_once '../config/dbconnection.php';
include_once '../objects/member.php';

// instantiate database and member object
$database = new dbconnection();
$db = $database->getConnection();

//initialize object

$member = new member($db);

//set ID property of the record to read

$member->id = isset($_GET['id']) ? $_GET['id'] : die();

//read one member information
$member->readOneMember();

if ($member->member_name != null) {
    //create array
    $member_field = array(
        "id" => $member->id,
        "member_name" => $member->member_name,
        "member_email" => $member->member_email,
        "member_phone" => $member->member_phone,
        "member_password" => $member->member_password,
        "member_role" => $member->member_role,
        "member_status" => $member->member_status
    );
    // set response code - 200 ok
    http_response_code(200);
    echo json_encode($member_field);
} else {
    //set response code - 404 not found
    http_response_code(404);
    //tell the user

    echo json_encode(array("message" => "member does not exist"));
}
