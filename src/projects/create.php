<?php
include_once '../config/dbconnection.php';
include_once '../objects/project.php';

$database = new dbconnection();
$db = $database->getConnection();

$project = new project($db);

$data =  json_decode(file_get_contents("php://input"));

if (
    !empty($data->project_name) &&
    !empty($data->project_budget) &&
    !empty($data->start_date) &&
    !empty($data->desired_end_date) &&
    !empty($data->project_area) &&
    !empty($data->actual_completion_date) &&
   // !empty($data->project_status) &&
    !empty($data->project_description) &&
    !empty($data->client_name) &&
    !empty($data->client_phone) &&
    !empty($data->client_email) &&
    !empty($data->client_organization) &&
    !empty($data->client_houseno) &&
    !empty($data->client_city) &&
    !empty($data->client_country)
) {

    $project->project_name = $data->project_name;
    // echo json_encode($project->project_name);
    $project->project_budget = $data->project_budget;
    $project->start_date = $data->start_date;
    $project->desired_end_date = $data->desired_end_date;
    $project->project_area = $data->project_area;
    $project->actual_completion_date = $data->actual_completion_date;
    $project->project_status = $data->project_status;
    // $project->percent_complete = $data->percent_complete;
    $project->project_description = $data->project_description;
    $project->client_name = $data->client_name;
    $project->client_phone = $data->client_phone;
    $project->client_email = $data->client_email;
    $project->client_organization = $data->client_organization;
    $project->client_houseno = $data->client_houseno;
    $project->client_city = $data->client_city;
    $project->client_country = $data->client_country;
    // echo $project->project_name;
    if ($project->create()) {
        http_response_code(201);
        echo json_encode(array("project created"));
    } else {
        http_response_code(503);
        echo json_encode(array("unable to create project"));
    }
} else {
    http_response_code(400);
    echo json_encode(array("Unable to create project . Data is incomplete"));
}
