<?php
include_once '../config/dbconnection.php';
include_once '../objects/project.php';

$database = new dbconnection();
$db = $database->getConnection();

$project = new project($db);

$data =  json_decode(file_get_contents("php://input"));

// set id property of member to be edited
    $project->id = $data->id;

   
// set property values of member

    $project->project_name = $data->project_name;
    $project->project_budget = $data->project_budget;
    $project->start_date = $data->start_date;
    $project->desired_end_date = $data->desired_end_date;
    $project->project_area = $data->project_area;
    $project->actual_completion_date = $data->actual_completion_date;
    $project->project_status = $data->project_status;
    $project->percent_complete = $data->percent_complete;
    $project->project_description = $data->project_description;
    $project->client_name = $data->client_name;
    $project->client_phone = $data->client_phone;
    $project->client_email = $data->client_email;
    $project->client_organization = $data->client_organization;
    $project->client_houseno = $data->client_houseno;
    $project->client_city = $data->client_city;
    $project->client_country = $data->client_country;
   
    if ($project->update()) {
        http_response_code(201);
        echo json_encode(array("project updated"));
    } else {
        http_response_code(503);
        echo json_encode(array("unable to update project"));
    }

