<?php
include_once '../config/dbconnection.php';
include_once '../objects/project.php';

// instantiate database and project object
$database = new dbconnection();
$db = $database->getConnection();

//initialize object

$project = new project($db);

//set ID property of the record to read

$project->id = isset($_GET['id']) ? $_GET['id'] : die();

//read one project information
$project->readoneproject();

if ($project->project_name != null) {

    //create array
    $project_field = array(
        "id" => $project->id,
        "project_name" => $project->project_name,
        "project_budget" => $project->project_budget,
        "start_date" => $project->start_date,
        "desired_end_date" => $project->desired_end_date,
        "project_area" => $project->project_area,
        "actual_completion_date" => $project->actual_completion_date,
        "project_status" => $project->project_status,
        "percent_complete" => $project->percent_complete,
        "project_description" => $project->project_description,
        "client_name" => $project->client_name,
        "client_phone" => $project->client_phone,
        "client_email" => $project->client_email,
        "client_organization" => $project->client_organization,
        "client_houseno" => $project->client_houseno,
        "client_city" => $project->client_city,
        "client_country" => $project->client_country,
    );
    // set response code - 200 ok
    http_response_code(200);
    echo json_encode($project_field);
} else {
    //set response code - 404 not found
    http_response_code(404);
    //tell the user

    echo json_encode(array("message" => "project does not exist"));
}
