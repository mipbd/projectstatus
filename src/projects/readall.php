<?php
include_once '../config/dbconnection.php';
include_once '../objects/project.php';

// instantiate database and project object
$database = new dbconnection();
$db = $database->getConnection();

//initialize object

$project = new project($db);

//read projects will be here
// query projects

$statement = $project->readall();
$num = $statement->rowCount();

//check if more thar 0 records found

if ($num > 0) {
    //project array

    $project_arr = array();
    $project_arr["records"] = array();

    //retrieve our table contents
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
        //extract row 
        //this will make $row['project_name'] to just $project_name
        extract($row);

        $project_feild = array(
            "id" => $id,
            "project_name" => $project_name,
            "project_budget" => $project_budget,
            "start_date" => $start_date,
            "desired_end_date" => $desired_end_date,
            "project_area" => $project_area,
            "actual_completion_date" => $actual_completion_date,
            "project_status" => $project_status,
            "percent_complete" => $percent_complete,
            "project_description"=>$project_description,
            "client_name"=>$client_name,
            "client_phone"=>$client_phone,
            "client_email"=>$client_email,
            "client_organization"=>$client_organization,
            "client_houseno"=>$client_houseno,
            "client_city"=>$client_city,
            "client_country"=>$client_country,
        );
        array_push($project_arr["records"], $project_feild);
    }
    // set response code - 200 OK
    http_response_code(200);

    // show projects data in json format
    echo json_encode($project_arr);
}

// no projects found will be here
else {

    // set response code - 404 Not found
    http_response_code(404);

    // tell the user no projects found
    echo json_encode(
        array("message" => "No project found.")
    );
}
