<?php
 
// include database and object file
include_once '../config/dbconnection.php';
include_once '../objects/project.php';
  
// get database connection
$database = new dbconnection();
$db = $database->getConnection();
  
// prepare Project object
$project = new project($db);
  
// get project id
$data = json_decode(file_get_contents("php://input"));
  
// set project id to be deleted
$project->id = $data->id;
  
// delete the project
if($project->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "project was deleted."));
}
  
// if unable to delete the project
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete project."));
}
