<?php
//credit: https://www.codeofaninja.com/2017/02/create-simple-rest-api-in-php.html
// include '../../.env';

class dbconnection
{

    private $conn;
    // protected $host = getenv('DBHOST');

    public $host = "localhost";
    public $username = "root";
    public $password = "";
    public $dbname = "project_management";
    public function __construct()
    {
        // echo "in constructor of dbconnection class<br>";
    }

    // get the database connection
    public function getConnection()
    {
        $this->conn = null;
        $options = [
            PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
        ];
        try {
            // $this->conn = new PDO("mysql:host=" . getenv('DBHOST') . ";dbname=" . getenv('DBNAME') . "", getenv('DBUSER'), getenv('DBPASS'), $options);
            $this->conn = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username, $this->password, $options);
        } catch (PDOException $exception) {
            throw $exception;
        }

        return $this->conn;
    }
}
