<?php
class dblayer
{
    private $dbconn = null;
    public function __construct()
    {
        try 
        {
            $db = new dbconnection();
            echo 'DBCONN object created<br>';
            $this->dbconn = $db->getConnection();
        }
        catch(Exception $exception)
        {
            //log error details here and then throw
            throw new Exception("Cannot open connection to database: " . $exception->getMessage());
        }
    }

    /**
     * for login
     * a token will be returned to the caller
     * remove token creation from here and move it to the caller 
     * TODO - ths way dblayer stays cleaner and any other non-token based process can use it. 
     */
    public function login($username, $pass)
    {
        try
        {
            //check login
           $stmt = $this->dbconn->prepare("select * from members where member_email = :email and member_password = PASSWORD(:pass)");
           $stmt->execute([':email' => $username, ':pass' => $pass]);
           $row = $stmt->fetchAll();
           if($row)
           {
                return jwtcreator::getToken($username, $pass);
           }
           else
           {
                return 'FALTU';//      
           }
        }   
        catch(Exception $exception)
        {
            throw $exception;
        }
    } 

    /**
     * returns all the projects - no member info are returned
     * returns an array 
     */
    public function getAllProjects()
    {
        $projects = [];
        try
        {
           return $projects;
        }
        catch(Exception $e)
        {

        }
    }

    /**
     * Returns a project with the specified project id
     * It also returns the members along with the project info
     * Returnns a project object
     */
    public function getProjectByID($projectid)
    {
        return null;
    }    

    /** 
     * Returns all the members for the project identified by the id
     * returns array of member objects 
    */
    public function getProjectMembers($projectid)
    {
        $members = [];
    }

    /**
     * Reset password token is saved for the member.
     */
    public function setResetPassToken($user, $token)
    {

    }   

    /**
     * Updates the password of the user specified
     */
    public function updatePassword($user, $password)
    {

    }

    /**
     * sets the status of the project identified by the id
     *  set by project manager level user.
     */
    public function setProjectStatus($status, $prjid)
    {

    }

    /**
     * sets the precentage completed of a project
     * set by project manager level user.
     */
    public function setPercentComple($percentconmplete, $prjid)
    {

    }
 
    /**
     * sets the active status of the member
     * called by admin user only.
     * 
     */
    public function setMemberStatus($memberid, $status)
    {

    }
}