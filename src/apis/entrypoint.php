<?php
class entrypoint
{
    public static function getBDLayer()
    {
        try
        {
            $dblayer = new dblayer();
            return $dblayer;
        }
        catch(Exception $exception)
        {
            //log error here.  Then throw
            throw $exception;
        }
    }

    /**
     * for login
     * a token will be returned to the caller
     */
    public static function login($username, $pass)
    {
        $res = new response();
        try
        {
            $dblayer = entrypoint::getBDLayer();
            $retunrval = $dblayer->login($username, $pass);
            $res->token = $retunrval;
            echo json_encode($res);
        }   
        catch(Exception $exception)
        {
            //first log this
            echo $exception->errorMessage();
        }
    } 

    /**
     * for login with token - will be called in subsequent operations after first login with username and login
     */
    public static function loginWithToken($token)
    {
        try
        {
            jwtcreator::verifyToken($token);
        }   
        catch(tokenexpiredexception $texception)
        {
            //log error first
            //will send json error
            echo $texception->errorMessage();
        }
        catch(invalidsignexception $isexception)
        {
            echo $isexception->errorMessage();
        }        
    } 

    /**
     * for initial request to reset pass
     * will return a token in the mail.
     */
    public static function resetPasswordRequest($user)
    {
        //first check if user is valid
        //generate token and call dblayer to save the token
        //then send email with the token
        try
        {
            $dblayer = self::getDBLayer();

        }   
        catch(Exception $exception)
        {
            echo $exception->errorMessage();
        }    
    }

    /**
     * based on a valid reset token, the password will be reset.
     */
    public static function updatePassword($user, $token, $newpass, $matchnewpass)
    {
        //first check if newpass and newpassmatch matches
        //then check if the token exists and is correct for the user
        //then allow resetting pass
        try
        {
            $dblayer = self::getDBLayer();

        }   
        catch(Exception $exception)
        {
            echo $exception->getMessage();
        }    

    }

    /**
     * will return list of projects 
     * but memners wont be included.
     */
    public static function getProjects($token)
    {
        $res = new response();
        try
        {
            jwtcreator::verifyToken($token);
            $dblayer = entrypoint::getBDLayer();
            $projects = $dblayer->getAllProjects();
            foreach($projects as $p)
            {
                //fill in project info
                //then push into the array of response object
            }

            //test data
            $p1 = new projects();
            $p1->id = 1;
            $p1->project_name = 'Project 1';
            $p1->client = 'BIWTC';
            $p1->start_date = '2020-10-10';
            $p1->end_date = '2020-10-10';;
            $p1->project_area = 'private'; 
            //add members
            $p2 = new projects();
            $p2->id = 1;
            $p2->project_name = 'Project 2';
            $p2->client = 'FISHERIES';
            $p2->start_date = '2020-07-07';
            $p2->end_date = '2020-10-10';;
            $p2->project_area = 'publci';
            array_push($res->listofthings, $p1);
            array_push($res->listofthings, $p2);             
            echo json_encode($res);
        }  
        catch(tokenexpiredexception $texception)
        {
            //log error first
            //will send json error
            $res->error = $texception->errorMessage();
            echo json_encode($res);//$texception->errorMessage();
        }
        catch(invalidsignexception $isexception)
        {
            $res->error = $isexception->errorMessage();
            echo json_encode($res);//$texception->errorMessage();            
            //echo $isexception->errorMessage();
        }        

        catch(Exception $exception)
        {
            $res->error = $exception->errorMessage();
            echo json_encode($res);        
        }
    }

    public static function getProjectByID($prjid)
    {
        try
        {
            $dblayer = self::getDBLayer();

        }   
        catch(Exception $exception)
        {
            echo $exception->getMessage();
        }
    }

    public static function getAllMembers($prjid)
    {
        try
        {
            $dblayer = self::getDBLayer();
        }   
        catch(Exception $exception)
        {
            echo $exception->getMessage();
        }
    }

    public static function updateProjectStatus($prjid, $status)
    {
        try
        {
            $dblayer = self::getDBLayer();

        }   
        catch(Exception $exception)
        {
            echo $exception->getMessage();
        }
    }

    public static function updatePercentComplete($prjid, $precentcomplete)
    {
        try
        {
            $dblayer = self::getDBLayer();
        }   
        catch(Exception $exception)
        {
            echo $exception->getMessage();
        }
    }

    public static function updateMemberStatus ($memberid, $status)
    {
        try
        {
            $dblayer = self::getDBLayer();

        }   
        catch(Exception $exception)
        {
            echo $exception->getMessage();
        }
    }

    public static function sendEmail($from, $to, $subject, $body)
    {
        try
        {
            $dblayer = self::getDBLayer();

        }   
        catch(Exception $exception)
        {
            echo $exception->getMessage();
        }
    }
}
