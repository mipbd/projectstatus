<?php 
include '../models/projects.php';
class project extends projects{

    //database connection and table name

    public $conn;
    public $table_name ="projects";

    public function __construct($db)
    {
        $this->conn =  $db;
    }


    public function readall()
    {
        $query = "SELECT * FROM `$this->table_name`";
        $statement = $this->conn->prepare($query);
        $statement->execute();
        return $statement;
    }

    public function readoneproject()
    {
        $query = "SELECT * FROM `$this->table_name` WHERE id=:id";
        //prepare statement

        $statement = $this->conn->prepare($query);
        //bind value 
        $statement->bindParam(':id', $this->id);
        //execute query
        $statement->execute();


        //get retrive data
        $row = $statement->fetch(PDO::FETCH_ASSOC);

        //set values to object property
        $this->id = $row['id'];
        $this->project_name = $row['project_name'];
        $this->project_budget = $row['project_budget'];
        $this->start_date = $row['start_date'];
        $this->desired_end_date = $row['desired_end_date'];
        $this->project_area = $row['project_area'];
        $this->actual_completion_date = $row['actual_completion_date'];
        $this->project_status = $row['project_status'];
        $this->percent_complete = $row['percent_complete'];
        $this->project_description = $row['project_description'];
        $this->client_name = $row['client_name'];
        $this->client_phone = $row['client_phone'];
        $this->client_email = $row['client_email'];
        $this->client_organization = $row['client_organization'];
        $this->client_houseno = $row['client_houseno'];
        $this->client_city = $row['client_city'];
        $this->client_country = $row['client_country'];

    }

    public function create()
    {
        $query = "INSERT INTO `$this->table_name` (project_name,project_budget,start_date,desired_end_date,project_area,actual_completion_date,project_status,percent_complete,project_description,client_name,client_phone,client_email,client_organization,client_houseno,client_city,client_country) VALUES(:project_name,:project_budget,:start_date,:desired_end_date,:project_area,:actual_completion_date,:project_status,:percent_complete,:project_description,:client_name,:client_phone,:client_email,:client_organization,:client_houseno,:client_city,:client_country)";

        //preare query

        $statement = $this->conn->prepare($query);

        //sanitize

        $this->project_name = htmlspecialchars(strip_tags($this->project_name));
        $this->project_budget = htmlspecialchars(strip_tags($this->project_budget));
        $this->start_date = htmlspecialchars(strip_tags($this->start_date));
        $this->desired_end_date = htmlspecialchars(strip_tags($this->desired_end_date));
        $this->project_area = htmlspecialchars(strip_tags($this->project_area));
        $this->actual_completion_date = htmlspecialchars(strip_tags($this->actual_completion_date));
        $this->project_status = htmlspecialchars(strip_tags($this->project_status));
        $this->percent_complete = htmlspecialchars(strip_tags($this->percent_complete));
        $this->project_description = htmlspecialchars(strip_tags($this->project_description));
        $this->client_name = htmlspecialchars(strip_tags($this->client_name));
        $this->client_phone = htmlspecialchars(strip_tags($this->client_phone));
        $this->client_email = htmlspecialchars(strip_tags($this->client_email));
        $this->client_organization = htmlspecialchars(strip_tags($this->client_organization));
        $this->client_houseno = htmlspecialchars(strip_tags($this->client_houseno));
        $this->client_city = htmlspecialchars(strip_tags($this->client_city));
        $this->client_country = htmlspecialchars(strip_tags($this->client_country));


        //bind values

        $statement->bindParam(":project_name",$this->project_name);
        $statement->bindParam(":project_budget",$this->project_budget);
        $statement->bindParam(":start_date",$this->start_date);
        $statement->bindParam(":desired_end_date",$this->desired_end_date);
        $statement->bindParam(":project_area",$this->project_area);
        $statement->bindParam(":actual_completion_date",$this->actual_completion_date);
        $statement->bindParam(":project_status",$this->project_status);
        $statement->bindParam(":percent_complete",$this->percent_complete);
        $statement->bindParam(":project_description",$this->project_description);
        $statement->bindParam(":client_name",$this->client_name);
        $statement->bindParam(":client_phone",$this->client_phone);
        $statement->bindParam(":client_email",$this->client_email);
        $statement->bindParam(":client_organization",$this->client_organization);
        $statement->bindParam(":client_houseno",$this->client_houseno);
        $statement->bindParam(":client_city",$this->client_city);
        $statement->bindParam(":client_country",$this->client_country);

        //execute query

        if ($statement->execute()) {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function update()
    {
        $query = "UPDATE `$this->table_name` SET project_name=:project_name,project_budget=:project_budget,start_date=:start_date,desired_end_date=:desired_end_date,project_area=:project_area,actual_completion_date=:actual_completion_date,project_status=:project_status,percent_complete=:percent_complete,project_description=:project_description,client_name=:client_name,client_phone=:client_phone,client_email=:client_email,client_organization=:client_organization,client_houseno=:client_houseno,client_city=:client_city,client_country=:client_country WHERE id=:id";

        //preare query

        $statement = $this->conn->prepare($query);

          //sanitize
          $this->id=htmlspecialchars(strip_tags($this->id));
          $this->project_name = htmlspecialchars(strip_tags($this->project_name));
          $this->project_budget = htmlspecialchars(strip_tags($this->project_budget));
          $this->start_date = htmlspecialchars(strip_tags($this->start_date));
          $this->desired_end_date = htmlspecialchars(strip_tags($this->desired_end_date));
          $this->project_area = htmlspecialchars(strip_tags($this->project_area));
          $this->actual_completion_date = htmlspecialchars(strip_tags($this->actual_completion_date));
          $this->project_status = htmlspecialchars(strip_tags($this->project_status));
          $this->percent_complete = htmlspecialchars(strip_tags($this->percent_complete));
          $this->project_description = htmlspecialchars(strip_tags($this->project_description));
          $this->client_name = htmlspecialchars(strip_tags($this->client_name));
          $this->client_phone = htmlspecialchars(strip_tags($this->client_phone));
          $this->client_email = htmlspecialchars(strip_tags($this->client_email));
          $this->client_organization = htmlspecialchars(strip_tags($this->client_organization));
          $this->client_houseno = htmlspecialchars(strip_tags($this->client_houseno));
          $this->client_city = htmlspecialchars(strip_tags($this->client_city));
          $this->client_country = htmlspecialchars(strip_tags($this->client_country));

             //bind values
        $statement->bindParam(':id', $this->id);
        $statement->bindParam(":project_name",$this->project_name);
        $statement->bindParam(":project_budget",$this->project_budget);
        $statement->bindParam(":start_date",$this->start_date);
        $statement->bindParam(":desired_end_date",$this->desired_end_date);
        $statement->bindParam(":project_area",$this->project_area);
        $statement->bindParam(":actual_completion_date",$this->actual_completion_date);
        $statement->bindParam(":project_status",$this->project_status);
        $statement->bindParam(":percent_complete",$this->percent_complete);
        $statement->bindParam(":project_description",$this->project_description);
        $statement->bindParam(":client_name",$this->client_name);
        $statement->bindParam(":client_phone",$this->client_phone);
        $statement->bindParam(":client_email",$this->client_email);
        $statement->bindParam(":client_organization",$this->client_organization);
        $statement->bindParam(":client_houseno",$this->client_houseno);
        $statement->bindParam(":client_city",$this->client_city);
        $statement->bindParam(":client_country",$this->client_country);

        //execute query

        if ($statement->execute()) {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function delete()
    {
         // delete query
         $query = "DELETE FROM `$this->table_name` WHERE id = ?";

         // prepare query
         $stmt = $this->conn->prepare($query);
 
         // sanitize
         $this->id = htmlspecialchars(strip_tags($this->id));
 
         // bind id of record to delete
         $stmt->bindParam(1, $this->id);
 
         // execute query
         if ($stmt->execute()) {
             return true;
         }
 
         return false;
    }



}
