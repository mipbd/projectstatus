<?php 
include '../models/project_members.php';
class project_member extends project_members{

//


 // database connection and table name
 public $conn;
 public $table_name = "project_members";

 public function __construct($db)
 {
     $this->conn = $db;
 }

 public function readall()
 {
    $query = "SELECT pm.id,m.member_name,m.member_email,m.member_phone,pm.member_role ,m.member_status,p.project_name,p.start_date,p.desired_end_date,p.project_area,p.actual_completion_date,p.project_status,p.percent_complete,p.project_description from project_members pm INNER join projects p on pm.project_id= p.id INNER JOIN members m on pm.member_id=m.id";
    $statement = $this->conn->prepare($query);
    $statement->execute();
    return $statement;
 }

 public function readoneprojectmember()
 {
    $query = "SELECT pm.id,m.member_name,m.member_email,m.member_phone,pm.member_role ,m.member_status,p.project_name,p.start_date,p.desired_end_date,p.project_area,p.actual_completion_date,p.project_status,p.percent_complete,p.project_description from project_members pm INNER join projects p on pm.project_id= p.id INNER JOIN members m on pm.member_id=m.id  WHERE pm.id=:id";
    //prepare statement

    $statement = $this->conn->prepare($query);
    //bind value 
    $statement->bindParam(':id', $this->id);
    //execute query
    $statement->execute();
return $statement;
 }

 public function create()
 {
    
    $query = "INSERT into `$this->table_name` (project_id,member_id,member_role) VALUES (:project_id ,:member_id,:member_role)";

    //prepare query
    $statement = $this->conn->prepare($query);

    //sanitize
    $this->project_id = htmlspecialchars(strip_tags($this->project_id));
    $this->member_id = htmlspecialchars(strip_tags($this->member_id));
    $this->member_role = htmlspecialchars(strip_tags($this->member_role));
   


    //bind values
    $statement->bindParam(":project_id", $this->project_id);
    $statement->bindParam(":member_id", $this->member_id);
    $statement->bindParam(":member_role", $this->member_role);
   

    //execute query

    if ($statement->execute()) {
        return true;
    } else {
        return false;
    }
 } 
 public function update()
 {
    $query = "UPDATE `$this->table_name` SET project_id=:project_id, member_id=:member_id,member_role=:member_role WHERE id= :id";

    // prepare query statement
    $statement = $this->conn->prepare($query);

    //sanitize
    $this->id = htmlspecialchars(strip_tags($this->id));
    $this->project_id = htmlspecialchars(strip_tags($this->project_id));
    $this->member_id = htmlspecialchars(strip_tags($this->member_id));
    $this->member_role = htmlspecialchars(strip_tags($this->member_role));
   


    //bind the new values
    $statement->bindParam(':id', $this->id);
    $statement->bindParam(':project_id', $this->project_id);
    $statement->bindParam(':member_id', $this->member_id);
    $statement->bindParam(':member_role', $this->member_role);
   
    //execute the query

    if ($statement->execute()) {
        return true;
    } else {
        return false;
    }
 }
public function delete()
{
  
        // delete query
        $query = "DELETE FROM `$this->table_name` WHERE id = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->id = htmlspecialchars(strip_tags($this->id));

        // bind id of record to delete
        $stmt->bindParam(1, $this->id);

        // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;
}

}

?>