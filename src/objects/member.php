<?php
include '../models/members.php';

class member extends members
{
    // database connection and table name
    public $conn;
    public $table_name = "members";

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function readall()
    {

        $query = "SELECT * FROM `$this->table_name`";
        $statement = $this->conn->prepare($query);
        $statement->execute();
        return $statement;
    }
    function readOneMember()
    {

        $query = "SELECT * FROM `$this->table_name` WHERE id=:id";
        //prepare statement

        $statement = $this->conn->prepare($query);
        //bind value 
        $statement->bindParam(':id', $this->id);
        //execute query
        $statement->execute();


        //get retrive data
        $row = $statement->fetch(PDO::FETCH_ASSOC);

        //set values to object property
        $this->id = $row['id'];
        $this->member_name = $row['member_name'];
        $this->member_email = $row['member_email'];
        $this->member_phone = $row['member_phone'];
        $this->member_password = $row['member_password'];
        $this->member_role = $row['member_role'];
        $this->member_status = $row['member_status'];
    }

    function create()
    {
        $query = "INSERT into `$this->table_name` (member_name,member_email,member_phone,member_password,member_role,member_status) VALUES (:member_name ,:member_email,:member_phone,:member_password,:member_role,:member_status)";

        //prepare query
        $statement = $this->conn->prepare($query);

        //sanitize
        $this->member_name = htmlspecialchars(strip_tags($this->member_name));
        $this->member_email = htmlspecialchars(strip_tags($this->member_email));
        $this->member_phone = htmlspecialchars(strip_tags($this->member_phone));
        $this->member_password = htmlspecialchars(strip_tags($this->member_password));
        $this->member_role = htmlspecialchars(strip_tags($this->member_role));
        $this->member_status = htmlspecialchars(strip_tags($this->member_status));


        //bind values
        $statement->bindParam(":member_name", $this->member_name);
        $statement->bindParam(":member_email", $this->member_email);
        $statement->bindParam(":member_phone", $this->member_phone);
        $statement->bindParam(":member_password", $this->member_password);
        $statement->bindParam(":member_role", $this->member_role);
        $statement->bindParam(":member_status", $this->member_status);

        //execute query

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }

    function update()
    {
        $query = "UPDATE `$this->table_name` SET member_name=:member_name, member_email=:member_email,member_phone=:member_phone,member_password=:member_password,member_role=:member_role,member_status=:member_status WHERE id=:id";

        // prepare query statement
        $statement = $this->conn->prepare($query);

        //sanitize
        $this->id = htmlspecialchars(strip_tags($this->id));
        $this->member_name = htmlspecialchars(strip_tags($this->member_name));
        $this->member_email = htmlspecialchars(strip_tags($this->member_email));
        $this->member_phone = htmlspecialchars(strip_tags($this->member_phone));
        $this->member_password = htmlspecialchars(strip_tags($this->member_password));
        $this->member_role = htmlspecialchars(strip_tags($this->member_role));
        $this->member_status = htmlspecialchars(strip_tags($this->member_status));



        //bind the new values
        $statement->bindParam(':id', $this->id);
        $statement->bindParam(':member_name', $this->member_name);
        $statement->bindParam(':member_email', $this->member_email);
        $statement->bindParam(':member_phone', $this->member_phone);
        $statement->bindParam(':member_password', $this->member_password);
        $statement->bindParam(':member_role', $this->member_role);
        $statement->bindParam(':member_status', $this->member_status);

        //execute the query

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }
    }
    function delete()
    {

        // delete query
        $query = "DELETE FROM `$this->table_name` WHERE id = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->id = htmlspecialchars(strip_tags($this->id));

        // bind id of record to delete
        $stmt->bindParam(1, $this->id);

        // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;
    }
}
