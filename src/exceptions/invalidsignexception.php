<?php
class invalidsignexception extends Exception {
  public function errorMessage() {
    //error message
    return "Token's signature is invalid or token format is incorrect!";
  }
}