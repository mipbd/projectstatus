<?php

include_once '../config/dbconnection.php';
include_once '../objects/project_member.php';

$database = new dbconnection();
$db = $database->getConnection();

$project_member = new project_member($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->project_id) &&
    !empty($data->member_id) &&
    !empty($data->member_role)
) {
    $project_member->project_id = $data->project_id;
    $project_member->member_id = $data->member_id;
    $project_member->member_role = $data->member_role;
   

    if ($project_member->create()) {
        http_response_code(201);
        echo json_encode(array("project member created")); ////
    } else {
        http_response_code(503);
        echo json_encode(array("unable to create project member"));
    }
} else {
    http_response_code(400);
    echo json_encode(array("Unable to create project member. Data is incomplete"));
}
