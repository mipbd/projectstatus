<?php
 
// include database and object file
include_once '../config/dbconnection.php';
include_once '../objects/project_member.php';
  
// get database connection
$database = new dbconnection();
$db = $database->getConnection();
  
// prepare project member object
$project_member = new project_member($db);
  
// get project member id
$data = json_decode(file_get_contents("php://input"));
  
// set project member id to be deleted
$project_member->id = $data->id;
  
// delete the project memeber
if($project_member->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "project member was deleted."));
}
  
// if unable to delete the project member
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete project member."));
}
