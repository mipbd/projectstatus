<?php
include_once '../config/dbconnection.php';
include_once '../objects/project_member.php';

// instantiate database and project member object
$database = new dbconnection();
$db = $database->getConnection();

//initialize object

$project_member = new project_member($db);

//set ID property of the record to read

$project_member->id = isset($_GET['id']) ? $_GET['id'] : die();

//read one project member information
$statement = $project_member->readoneprojectmember();
$num = $statement->rowCount();

//check if more thar 0 records found

if ($num > 0) {
    //project member array

    $project_member_array = array();
    $project_member_array["records"] = array();

    //retrieve our table contents
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
        //extract row 
        //this will make $row['member_name'] to just $member_name
        extract($row);

        $project_member_field = array(
            "id" => $id,
            "member_name" => $member_name,
            "member_email" => $member_email,
            "member_phone" => $member_phone,
            "member_role" => $member_role,
            "member_status" => $member_status,
            "project_name"=> $project_name,
            "start_date" =>$start_date,
            "desired_end_date" =>$desired_end_date,
            "project_area" =>$project_area,
            "actual_completion_date" =>$actual_completion_date,
            "project_status" =>$project_status,
            "percent_complete" =>$percent_complete,
            "project_description" =>$project_description
        );
        array_push($project_member_array["records"], $project_member_field);
    }
    // set response code - 200 OK
    http_response_code(200);

    // show project members data in json format
    echo json_encode($project_member_array);
}

// no project members found will be here
else {

    // set response code - 404 Not found
    http_response_code(404);

    // tell the user no project members found
    echo json_encode(
        array("message" => "No project member found.")
    );
}
// if ($project_member->member_name != null) {
//     //create array
//     $project_member_field = array(
//         "id" => $id,
//             "member_name" => $project_member->member_name,
//             "member_email" => $project_member->member_email,
//             "member_phone" => $project_member->member_phone,
//             "member_role" => $project_member->member_role,
//             "member_status" => $project_member->member_status,
//             "project_name"=> $project_member->project_name,
//             "start_date" =>$project_member->start_date,
//             "desired_end_date" =>$project_member->desired_end_date,
//             "project_area" =>$project_member->project_area,
//             "actual_completion_date" =>$project_member->actual_completion_date,
//             "project_status" =>$project_member->project_status,
//             "percent_complete" =>$project_member->percent_complete,
//             "project_description" =>$project_member->project_description
//     );
//     // set response code - 200 ok
//     http_response_code(200);
//     echo json_encode($project_member_field);
// } else {
//     //set response code - 404 not found
//     http_response_code(404);
//     //tell the user

//     echo json_encode(array("message" => "project member does not exist"));
// }
