<?php
include_once '../config/dbconnection.php';
include_once '../objects/project_member.php';

//get database connection
$database = new dbconnection();
$db = $database->getConnection();


//prepare project member object
$project_member = new project_member($db);

//get id of the project member to be edited

$data = json_decode(file_get_contents("php://input"));

//set id property of project member to be edited

$project_member->id = $data->id;

//set property  values of project member 

$project_member->project_id = $data->project_id;
$project_member->member_id = $data->member_id;
$project_member->member_role = $data->member_role;


//echo $project_member->project_id . $project_member->member_id.$project_member->member_role;

//update project  member information

if ($project_member->update()) {

    //set response to - 200 ok
    http_response_code(200);

    //tell the user 
    echo json_encode(array("message" => " project member is updated."));
}

//if unable to update the project  member , tell the user

else {
    //set response code -503 service unavailable
    http_response_code(503);

    //tell the user
    echo json_encode(array("message" => "Unable to update the project member."));
}
