<?php
class projects
{
	public $id;
	public $project_name;
	public $project_budget;
	public $start_date;
	public $desired_end_date;
	public $project_area;
	public $actual_completion_date;
	public $project_status;
	public $percent_complete;
	public $project_description;
	public $client_name;
	public $client_phone;
	public $client_email;
	public $client_organization;
	public $client_houseno;
	public $client_city;
	public $client_country;
}
