<?php
require 'vendor/autoload.php';
use Dotenv\Dotenv;

$dotenv = new DotEnv( __DIR__ );
$dotenv->load();

//courtesy of Andrew: 03-Nov-2006 12:26 [https://www.php.net/manual/en/language.oop5.autoload.php]
spl_autoload_register(function ($class_name) 
{
    //class directories
    $directorys = [
        'src/models/',
        'src/dblayer/',
        'src/utilities/',
        'src/config/',
        'src/apis/',
        'src/exceptions/'
    ];
   
    //for each directory
    foreach($directorys as $directory)
    {
        //see if the file exsists
        if(file_exists($directory.$class_name . '.php'))
        {
            require_once($directory.$class_name . '.php');
            //only require the class once, so quit after to save effort (if you got more, then name them something else
            return;
        }           
    }    

});
/*
$endpoints  = [ 
'access' => '',
'changepassinit' => '',
'changepass' => '',
'members' => '',
'membersforproject' => '',
'changememberstat' => '',
'projects' => '',
'projectbyid' => '',
'changeporjectstat' => '',
'setprojectpercent' => '',
];
*/

$BASEPATH = '/project_stat_api/index.php/';//make this "index.php/" if will sit on the main domain - SHOULD BE IN CONFIG

//now check route and forward to appropriate API
$requesturl = $_SERVER['REQUEST_URI'];
echo "URL - $requesturl<br>";
$urlparts = parse_url($requesturl);
print_r($urlparts);
echo '<br>';
$fullpath = $urlparts['path'];
echo "Full Path -> $fullpath<br>";
$endpoint = str_replace($BASEPATH,'',$fullpath);
echo "endpoint -> $endpoint<br>";
//API List
/**
access - for login - full path -> project_stat_api/index.php/access - will get credential through post data
changepassinit - for initiating a password change request
changepass - final password changing request with token received in the initial request.
members - see all members in the company
membersforproject - get all members of a certain project - proejct id will be posted.
changememberstat - changes the status of a member with memebr id sent in post
projects - get all projects
projectbyid - get a specfic project based on id - id will be sent in post data
changeporjectstat - change the status of a project with id
setprojectpercent - set percent completed of a project with id
 */
switch($endpoint)
{
    case 'access':
        access();
        break;
    case 'accesswtoken':
        accessWithToken();
        break;        
    case 'changepassinit':
        //
        break;
    case 'changepass':
        //
        break;
    case 'members':
        //
        break;
    case 'membersforproject':
        //
        break;
    case 'changememberstat':
        //
        break;
    case 'projects':
        //get all the projects
        getAllProjects();
        break;        
    case 'projectbyid':
        //
        break;
    case 'changeporjectstat':
        //
        break;
    case 'setprojectpercent':
        //
        break;
    default:
        echo 'No endpoint exists';          
}

//all functions
/**
 * this will be token based. 
 */
function access()
{
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");    
    echo "in access function of index<br>";
    //for testing
    $_POST['username'] = 'admin@blah.com';
    $_POST['pass'] = 'blah';
    $username = $_POST['username'];
    $pass = $_POST['pass'];
    if(!empty($username) && !empty($pass))
    {
        entrypoint::login($username, $pass);
    }
    else
    {
        echo "cannot provide token for empty username/password<br>";
    }
}

function accessWithToken()
{
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");    
    echo "in access function of index with token<br>";
    //for testing
    $_POST['token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW5AYmxhaC5jb20iLCJyb2xlIjoiYmxhaCIsImV4cCI6MTU5MzgyODIyMn0.XR6wKl5J7yo00pEq94pAS6Hx1rowU_RULkp2_n7tvcY.';//for testing only - will come from client
    $token = $_POST['token'];
    if(!empty($token))
    {
        entrypoint::loginWithToken($token);
    }
    else
    {
        echo "Empty token cannot be verified.<br>";
    }
}

function getAllProjects()
{
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");    
    echo "in get projects function of index<br>";
    //for testing
    $_POST['token'] = 'eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYWRtaW5AYmxhaC5jb20iLCJyb2xlIjoiYmxhaCIsImV4cCI6MTU5MzgyODIyMn0.XR6wKl5J7yo00pEq94pAS6Hx1rowU_RULkp2_n7tvcY.';//for testing only - will come from client
    $token = $_POST['token'];
    if(!empty($token))
    {
        entrypoint::getProjects($token);
    }
    else
    {
        echo "Empty token cannot be used.<br>";
    }
}