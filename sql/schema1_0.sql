-- DB Schema Mysql
-- members
CREATE TABLE IF NOT EXISTS members
(
	id smallint unsigned auto_increment,
	member_name varchar (100),
	member_email varchar (100) unique comment  'will be used as the login credential',
	member_phone varchar(15) unique,
	member_password varchar (50),
	member_role ENUM('ADMIN','PROJECT_MANAGER', 'TEAM_LEAD','MEMBER'),
	member_status ENUM('ACTIVE','INACTIVE'),
	primary key(id)		
);

-- projects
CREATE TABLE IF NOT EXISTS projects
(
	id smallint unsigned auto_increment,
	project_name varchar (100),
	client varchar (100),
	start_date datetime,
	end_date datetime,
	project_area enum('GOVERNMENT','PRIVATE'), 
	primary key(id)		
);

-- project members relation
CREATE TABLE IF NOT EXISTS project_members
(
	id smallint unsigned auto_increment,
	project_id smallint unsigned,
	member_id smallint unsigned,
	member_role ENUM('ADMIN','PROJECT_MANAGER', 'TEAM_LEAD','MEMBER'),
	foreign key (member_id) references members(id),	
	foreign key (project_id) references projects(id),
	primary key(id)		
);

-- default admin user
insert into members 
(member_name, member_email, member_phone, member_password,	member_role, member_status)
values
('ADMIN','admin@blah.com','012548792',PASSWORD('blah'),'ADMIN','ACTIVE');



