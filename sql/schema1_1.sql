-- DB Schema Mysql
-- alterations
alter table projects add column actual_completion_date datetime;
alter table projects add column status ENUM('COMPLETE', 'RUNNING', 'SUSPENDED');
alter table projects change column end_date desired_end_date datetime;
alter table projects add column percent_complete tinyint default 0 comment '0 - 100 will be set by project manager';




