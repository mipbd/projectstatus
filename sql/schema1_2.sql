-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2020 at 10:14 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `member_name` varchar(100) DEFAULT NULL,
  `member_email` varchar(100) DEFAULT NULL COMMENT 'will be used as the login credential',
  `member_phone` varchar(15) DEFAULT NULL,
  `member_password` varchar(50) DEFAULT NULL,
  `member_role` enum('ADMIN','PROJECT_MANAGER','TEAM_LEAD','MEMBER') DEFAULT NULL,
  `member_status` enum('ACTIVE','INACTIVE') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `member_name`, `member_email`, `member_phone`, `member_password`, `member_role`, `member_status`) VALUES
(1, 'Sifat Shahria', 'sifatul@gamil.com', '0178898989', '123', 'TEAM_LEAD', 'ACTIVE'),
(6, 'Mahfuzul Islam', 'mahfuzul@gamil.com', '017455656329', '123', 'PROJECT_MANAGER', 'ACTIVE'),
(7, 'Mobin Mohetuzzaman', 'mobin.dev@gamil.com', '01788945459', '123', 'ADMIN', 'ACTIVE'),
(8, 'Sadat Jubayer', 'Sadat.dev@gamil.com', '0174545945459', '123', 'TEAM_LEAD', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `project_name` varchar(100) DEFAULT NULL,
  `project_budget` int(15) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `desired_end_date` datetime DEFAULT NULL,
  `project_area` enum('GOVERNMENT','PRIVATE') DEFAULT NULL,
  `actual_completion_date` datetime DEFAULT NULL,
  `project_status` enum('STARTED','RUNNING','COMPLETED','SUSPENDED') DEFAULT NULL,
  `percent_complete` tinyint(4) DEFAULT '0' COMMENT '0 - 100 will be set by project manager',
  `project_description` text,
  `client_name` varchar(50) DEFAULT NULL,
  `client_phone` varchar(15) DEFAULT NULL,
  `client_email` varchar(50) DEFAULT NULL,
  `client_organization` varchar(100) DEFAULT NULL,
  `client_houseno` varchar(50) DEFAULT NULL,
  `client_city` varchar(25) DEFAULT NULL,
  `client_country` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_members`
--

CREATE TABLE `project_members` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `project_id` smallint(5) UNSIGNED DEFAULT NULL,
  `member_id` smallint(5) UNSIGNED DEFAULT NULL,
  `member_role` enum('ADMIN','PROJECT_MANAGER','TEAM_LEAD','MEMBER') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `member_email` (`member_email`),
  ADD UNIQUE KEY `member_phone` (`member_phone`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_members`
--
ALTER TABLE `project_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `project_id` (`project_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_members`
--
ALTER TABLE `project_members`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `project_members`
--
ALTER TABLE `project_members`
  ADD CONSTRAINT `project_members_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `project_members_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
